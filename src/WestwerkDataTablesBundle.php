<?php

namespace Westwerk\DataTablesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Westwerk\DataTablesBundle\DependencyInjection\Compiler\DataTablePass;

class WestwerkDataTablesBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new DataTablePass());
    }

}
