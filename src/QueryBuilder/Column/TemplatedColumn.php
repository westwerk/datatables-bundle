<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 06/10/15
 * Time: 23:30
 */

namespace Westwerk\DataTablesBundle\QueryBuilder\Column;

use Westwerk\DataTables\Core\Column\ColumnInterface;

/**
 * Class TemplatedColumn
 *
 * @package Westwerk\StuetzpunktBundle\DataTable\Support
 */
class TemplatedColumn extends FluentColumn
{

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var string
     */
    private $template;

    /**
     * The "make" named constructor enables the fluency. You can also
     * use "create" (see below), and be even cooler!!
     *
     * @param \Twig_Environment $twig
     * @param $name
     * @param array $options8
     * @return static
     */
    public static function make(\Twig_Environment $twig, $name, array $options = [])
    {
        $instance = new static($name, $options);

        return $instance->twig($twig);
    }

    /**
     * This is really just for fun.
     *
     * @param string $name
     * @param string $source
     * @param array $options
     * @return TemplatedColumnBrigde
     */
    public static function create($name, $source, $options = [])
    {
        return new TemplatedColumnBrigde($name, $source, $options);
    }

    /**
     * @return Column
     */
    protected function getInstance()
    {
        return new Column($this->name, $this->options);
    }

    /**
     * @return array
     */
    protected function getDefaultOptions()
    {
        return [
            'label' => sprintf('table.header.%s', $this->name)
        ];
    }

    /**
     * @param $twig
     *
     * @return $this
     */
    public function twig($twig)
    {
        $this->twig = $twig;

        return $this;
    }

    public function template($template) {
        $this->template = $template;

        return $this;
    }

}