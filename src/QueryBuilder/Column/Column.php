<?php

namespace Westwerk\DataTablesBundle\QueryBuilder\Column;

use Westwerk\DataTables\Core\Column\AbstractColumn;
use Westwerk\DataTablesBundle\QueryBuilder\Column\Filter\ColumnFilterInterface;

/**
 * Class Column
 *
 * @package Westwerk\DataTablesBundle\QueryBuilder\Column
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
class Column extends AbstractColumn
{

    /**
     * @var null|callable
     */
    protected $onSelect = null;

    /**
     * @var null|callable
     */
    protected $onColumnFilterExpression = null;

    /**
     * @var null|callable
     */
    protected $onGlobalFilterExpression = null;

    /**
     * @var null|callable
     */
    protected $onFilterExpression = null;


    /**
     * @var ColumnFilterInterface
     */
    protected $filter;

    /**
     * @return callable|null
     */
    public function getOnSelect()
    {

        return $this->onSelect;
    }

    /**
     * @param callable|null $onSelect
     */
    public function setOnSelect($onSelect)
    {

        $this->onSelect = $onSelect;
    }

    /**
     * @return callable|null
     */
    public function getOnColumnFilterExpression()
    {
        return $this->onColumnFilterExpression;
    }

    /**
     * @param callable|null $onColumnFilterExpression
     */
    public function setOnColumnFilterExpression($onColumnFilterExpression)
    {
        $this->onColumnFilterExpression = $onColumnFilterExpression;
    }

    /**
     * @return callable|null
     */
    public function getOnGlobalFilterExpression()
    {
        return $this->onGlobalFilterExpression;
    }

    /**
     * @param callable|null $onGlobalFilterExpression
     */
    public function setOnGlobalFilterExpression($onGlobalFilterExpression)
    {
        $this->onGlobalFilterExpression = $onGlobalFilterExpression;
    }

    /**
     * @param callable|null $onFilterExpression
     */
    public function setOnFilterExpression($onFilterExpression)
    {
        $this->onColumnFilterExpression = $onFilterExpression;
        $this->onGlobalFilterExpression = $onFilterExpression;
    }

    /**
     * @return ColumnFilterInterface
     */
    public function getFilter()
    {
        return $this->filter;
    }


}