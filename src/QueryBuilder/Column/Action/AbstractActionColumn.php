<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 06/03/15
 * Time: 22:20
 */

namespace Westwerk\DataTablesBundle\QueryBuilder\Column\Action;


use Westwerk\DataTablesBundle\QueryBuilder\Column\Column;

abstract class AbstractActionColumn extends Column
{

    /**
     * @var bool
     */
    protected $virtual = true;

    /**
     * @var boolean
     */
    protected $sortable = false;

    /**
     * @var boolean
     */
    protected $exportable = false;

    /**
     * @var array|null
     */
    protected $actions = null;

    /**
     * @param array $actions
     */
    public function setActions(array $actions)
    {
        $this->actions = $actions;
    }

    /**
     * @return array|null
     */
    public function getActions()
    {
        return $this->actions;
    }

}