<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 07/10/15
 * Time: 01:48
 */

namespace Westwerk\DataTablesBundle\QueryBuilder\Column;

/**
 * Class SimpleColumn
 *
 * @package Westwerk\DataTablesBundle\QueryBuilder\Column
 */
class SimpleColumn extends FluentColumn
{

    /**
     * @return Column
     */
    protected function getInstance()
    {
        return new Column($this->name, $this->options);
    }

    /**
     * @param $name
     * @param $source
     * @param array $options
     * @return FluentColumn
     */
    public static function create($name, $source, $options = [])
    {
        return (new static($name, $options))->with('source', $source);
    }

    /**
     * @return array
     */
    protected function getDefaultOptions()
    {
        return [
            'label' => sprintf('table.header.%s', $this->name)
        ];
    }

}