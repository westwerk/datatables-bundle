<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 07/10/15
 * Time: 01:11
 */

namespace Westwerk\DataTablesBundle\QueryBuilder\Column;

use Doctrine\ORM\QueryBuilder;
use Westwerk\DataTables\Core\Column\ColumnInterface;
use Westwerk\DataTables\Core\Column\Filter\ColumnFilterInterface;
use Westwerk\DataTablesBundle\QueryBuilder\Column\Filter\TextColumnFilter;

/**
 * Class FluentColumn
 *
 * @package Westwerk\StuetzpunktBundle\DataTable\Support
 */
abstract class FluentColumn
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $options;

    /**
     * @var ColumnInterface
     */
    private $instance;

    /**
     * FluentColumn constructor.
     *
     * @param string $name
     * @param array  $options
     */
    public function __construct($name, array $options = [])
    {
        $this->name    = $name;
        $this->options = array_merge($this->getDefaultOptions(), $options);
    }

    /**
     * You may overwrite this in your implementation.
     *
     * @return array
     */
    protected function getDefaultOptions()
    {
        return [];
    }

    /**
     *
     * @return ColumnInterface
     */
    abstract protected function getInstance();

    /**
     * @return TextColumnFilter
     */
    protected function getDefaultColumnFilter()
    {
        return new TextColumnFilter();
    }

    /**
     * @param $callback
     *
     * @return mixed
     */
    public function then($callback)
    {
        if (!$this->instance) {
            $this->build();
        }

        return call_user_func_array($callback, [$this->instance]);
    }

    /**
     * @return ColumnInterface
     */
    public function build()
    {
        $this->instance = $this->getInstance();

        return $this->instance;
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return $this
     */
    function __call($name, $arguments)
    {
        if (substr($name, 0, 2) === "on"
            && is_callable($arguments[0])
        ) {
            $this->options[$name] = $arguments[0];

            return $this;
        }
    }

    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return $this
     */
    public function with($key, $value)
    {
        $this->options[$key] = $value;

        return $this;
    }

    /**
     * This method sets the className option, and can set classes condition-
     * ally. You can pass a callable, as well as any expression. If you
     * pass a callable, it will receive the $classes as its argument.
     *
     * @param string|array $classes
     *
     * @param null $condition
     * @return $this
     */
    public function className($classes = '', $condition = null)
    {
        $classes = is_array($classes) ? $classes : explode(' ', $classes);
        $classes = implode(' ', $classes);

        if ($condition === null) {
            return $this->with('className', $classes);
        }

        if (is_callable($condition)) {
            return call_user_func_array($condition, [$classes])
                ?
                $this->with('className', $classes)
                :
                $this;
        }

        return $condition
            ?
            $this->with('className', $classes)
            :
            $this;
    }


    /**
     * @param null $field
     *
     * @return FluentColumn
     */
    public function sortable($field = null)
    {
        if ($field === null) {
            return $this->with('sortable', true);
        }

        return $this->with(
            'onSort',
            function (QueryBuilder $qb, ColumnInterface $column) use ($field) {
                $qb->addOrderBy(
                    $field,
                    $column->getSortDirection() == ColumnInterface::SORT_DIRECTION_ASC ? 'ASC' : 'DESC'
                );
            }
        );
    }

    /**
     * @return FluentColumn
     */
    public function unsortable()
    {
        return $this->with('sortable', false);
    }

    /**
     * @param $source
     *
     * @return FluentColumn
     */
    public function source($source)
    {
        return $this->with('source', $source);
    }

    /**
     * @param ColumnFilterInterface|null $filter
     *
     * @return FluentColumn
     */
    public function filterable(ColumnFilterInterface $filter = null)
    {
        return $this->with('filter', $filter ? $filter : $this->getDefaultColumnFilter());
    }

    /**
     * @return FluentColumn
     */
    public function unfilterable()
    {
        return $this->with('filter', null);
    }

    /**
     * @return FluentColumn
     */
    public function nullFilterable()
    {
        return $this->with('nullFilterable', true);
    }

    /**
     * @return mixed
     */
    public function skipOnSelect()
    {
        return $this->with(
            'onSelect',
            function (QueryBuilder $qb, ColumnInterface $column) {
                // no-op
            }
        );
    }

}