<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 07/10/15
 * Time: 00:21
 */

namespace Westwerk\DataTablesBundle\QueryBuilder\Column;
use Westwerk\DataTables\Core\Column\ColumnInterface;

/**
 * Class TemplatedColumnBrigde
 *
 * This exists for autocompletion.
 *
 * @package Westwerk\StuetzpunktBundle\DataTable\Support
 */
class TemplatedColumnBrigde
{

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $source;

    /**
     * @var array
     */
    private $options;

    /**
     * TemplatedColumnBrigde constructor.
     *
     * @param $instance
     */
    public function __construct($name, $source, $options)
    {
        $this->name = $name;
        $this->source = $source;
        $this->options = $options;
    }

    /**
     * @param \Twig_Environment $twig
     * @return TemplatedColumn
     */
    public function using(\Twig_Environment $twig, $template = null)
    {
        $instance = (new TemplatedColumn($this->name, $this->options))
            ->source($this->source)
            ->twig($twig);

        if ($template !== null) {
            return $instance->onGetValue(function (ColumnInterface $column, $model) use ($twig, $template) {
                return $twig->render(
                    $template,
                    ['entity' => $model[0]]
                );
            });
        }

        return $instance;
    }

}