<?php
/**
 * Created by PhpStorm.
 * User: nicoschneider
 * Date: 06/03/15
 * Time: 22:21
 */

namespace Westwerk\DataTablesBundle\QueryBuilder\Column;

use Westwerk\DataTables\Core\Column\ColumnInterface;
use Twig_Environment;
use Westwerk\DataTablesBundle\QueryBuilder\Column\Action\AbstractActionColumn;

/**
 * Class ActionColumn
 *
 * @package Westwerk\DataTablesBundle\QueryBuilder\Column
 */
class ActionColumn extends AbstractActionColumn {


    /**
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * @var string
     */
    protected $template = 'WestwerkDataTablesBundle:Column/Action:dropdown.html.twig';

    /**
     * @var null|string
     */
    protected $handlerTemplate = null;

    /**
     * @var string
     */
    protected $toggleLabel;

    /**
     * @param string            $name
     * @param null|array        $options
     * @param Twig_Environment $twig
     */
    public function __construct($name, $options = [], Twig_Environment $twig = null) {
        parent::__construct($name, $options);
        $this->twig = $twig;
    }

    /**
     * Pseudo-partial application to generate a callable
     * which includes this column's actions
     *
     * @return callable
     */
    public function getOnGetValue() {

        $actions = $this->getActions();
        $twig = $this->getTwig();

        return function (ColumnInterface $column, $model) use ($actions, $twig) {

            return $twig->render(
                $this->getTemplate(),
                [
                    'actions'     => array_filter($actions, function($action) use ($column, $model) {
                        if (array_key_exists('enabled', $action)) {
                            if (is_callable($action['enabled'])) {
                                return boolval(call_user_func($action['enabled'], $column, $model));
                            }
                            return boolval($action['enabled']);
                        }
                        return true;
                    }),
                    'entity'      => reset($model),
                    'toggleLabel' => ($this->getToggleLabel() !== null) ? $this->getToggleLabel() : null,
                ]
            );
        };
    }

    /**
     * @var null|callable
     */
    protected $onSelect = null;

    /**
     * @var null|callable
     */
    protected $onFilterExpression = null;

    /**
     * @return callable|null
     */
    public function getOnSelect() {

        return $this->onSelect;
    }

    /**
     * @param callable|null $onSelect
     */
    public function setOnSelect($onSelect) {

        $this->onSelect = $onSelect;
    }

    /**
     * @return callable|null
     */
    public function getOnFilterExpression() {

        return $this->onFilterExpression;
    }

    /**
     * @param callable|null $onFilterExpression
     */
    public function setOnFilterExpression($onFilterExpression) {

        $this->onFilterExpression = $onFilterExpression;
    }

    /**
     * @return boolean
     */
    public function isVirtual() {

        return $this->virtual;
    }

    /**
     * @param boolean $virtual
     */
    public function setVirtual($virtual) {

        $this->virtual = $virtual;
    }

    /**
     * @return string
     */
    public function getTemplate() {

        return $this->template;
    }

    /**
     * @param string $template
     */
    public function setTemplate($template) {

        $this->template = $template;
    }

    /**
     * @return null|string
     */
    public function getHandlerTemplate() {

        return $this->handlerTemplate;
    }

    /**
     * @param null|string $handlerTemplate
     */
    public function setHandlerTemplate($handlerTemplate) {

        $this->handlerTemplate = $handlerTemplate;
    }

    /**
     * @return string
     */
    public function getToggleLabel() {

        return $this->toggleLabel;
    }

    /**
     * @param string $toggleLabel
     */
    public function setToggleLabel($toggleLabel) {

        $this->toggleLabel = $toggleLabel;
    }

    /**
     * @return Twig_Environment
     */
    public function getTwig()
    {
        return $this->twig;
    }


}