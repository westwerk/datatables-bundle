<?php


namespace Westwerk\DataTablesBundle\QueryBuilder\Column\Filter;


use Doctrine\ORM\QueryBuilder;

class TextColumnFilter extends AbstractColumnFilter {

    /**
     * @var string
     */
    protected $template = 'WestwerkDataTablesBundle:Column/Filter:text.html.twig';

}