<?php


namespace Westwerk\DataTablesBundle\QueryBuilder\Column\Filter;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\ORM\QueryBuilder;

class DateColumnFilter extends TextColumnFilter {

    /**
     * @var string
     */
    protected $template = 'WestwerkDataTablesBundle:Column/Filter:date.html.twig';

    protected $displayFormat = 'yyyy-mm-dd';

    protected $valueFormat = 'Y-m-d';

    /**
     * @param QueryBuilder $queryBuilder
     * @param              $filterString
     * @param              $parameterName
     *
     * @return ExpressionBuilder
     */
    public function getColumnFilterExpression(QueryBuilder $queryBuilder, $filterString, $parameterName)
    {
        // Column filter
        $exp = $queryBuilder->expr();
        try {
            $date = \DateTime::createFromFormat($this->getValueFormat(), $filterString);
            $queryBuilder->setParameter($parameterName, $date->format('Y-m-d'));
            return $exp->eq(
                $this->column->getSource(),
                ':'.$parameterName
            );
        }
        catch (\Exception $e) {
            return parent::getColumnFilterExpression($queryBuilder, $filterString, $parameterName);
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param              $filterString
     * @param              $parameterName
     *
     * @return null
     */
    public function getGlobalFilterExpression(QueryBuilder $queryBuilder, $filterString, $parameterName) {
        //Don't filter on global table filter change
        return null;
    }

    /**
     * @return string
     */
    public function getDisplayFormat()
    {
        return $this->displayFormat;
    }

    /**
     * @param string $displayFormat
     */
    public function setDisplayFormat($displayFormat)
    {
        $this->displayFormat = $displayFormat;
    }

    /**
     * @return string
     */
    public function getValueFormat()
    {
        return $this->valueFormat;
    }

    /**
     * @param string $valueFormat
     */
    public function setValueFormat($valueFormat)
    {
        $this->valueFormat = $valueFormat;
    }



}