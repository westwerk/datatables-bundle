<?php


namespace Westwerk\DataTablesBundle\QueryBuilder\Column\Filter;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\ORM\QueryBuilder;

class BoolColumnFilter extends TextColumnFilter {

    /**
     * @var string
     */
    protected $template = 'WestwerkDataTablesBundle:Column/Filter:bool.html.twig';

    /**
     * @param QueryBuilder $queryBuilder
     * @param              $filterString
     * @param              $parameterName
     *
     * @return ExpressionBuilder
     */
    public function getColumnFilterExpression(QueryBuilder $queryBuilder, $filterString, $parameterName)
    {
        // Column filter
        $exp = $queryBuilder->expr();
        try {
            $queryBuilder->setParameter($parameterName, intval($filterString));
            return $exp->eq(
                $this->column->getSource(),
                ':'.$parameterName
            );
        }
        catch (\Exception $e) {
            return parent::getColumnFilterExpression($queryBuilder, $filterString, $parameterName);
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param              $filterString
     * @param              $parameterName
     *
     * @return null
     */
    public function getGlobalFilterExpression(QueryBuilder $queryBuilder, $filterString, $parameterName) {
        //Don't filter on global table filter change
        return null;
    }




}