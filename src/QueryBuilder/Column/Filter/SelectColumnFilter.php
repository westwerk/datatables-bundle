<?php


namespace Westwerk\DataTablesBundle\QueryBuilder\Column\Filter;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\ORM\QueryBuilder;

class SelectColumnFilter extends TextColumnFilter
{

    /**
     * @var string
     */
    protected $template = 'WestwerkDataTablesBundle:Column/Filter:select.html.twig';

    /**
     * @var bool
     */
    protected $multiple = false;

    /**
     * @var array
     */
    protected $items = [];

    /**
     * @var null
     */
    protected $field = null;

    /**
     * @return boolean
     */
    public function isMultiple()
    {
        return $this->multiple;
    }

    /**
     * @param boolean $multiple
     */
    public function setMultiple($multiple)
    {
        $this->multiple = $multiple;
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @param $filterStr
     *
     * @return array
     */
    protected function getSelectedItems($filterString)
    {
        $selected = [];
        $items    = $this->getItems();
        foreach (explode(',', $filterString) as $index) {
            if (array_key_exists($index, $items)) {
                $selected[$index] = $items[$index];
            }
        }
        return $selected;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param              $filterString
     * @param              $parameterName
     *
     * @return ExpressionBuilder
     */
    public function getColumnFilterExpression(QueryBuilder $queryBuilder, $filterString, $parameterName)
    {
        // Column filter
        $ORs = [];
        $exp = $queryBuilder->expr();

        $selected = $this->getSelectedItems($filterString);
        foreach ($selected as $index => $item) {
            $itemParameterName = $parameterName . '_' . $index;
            switch (strtolower(gettype($item['value']))) {
                case 'null':
                    //Do nothing
                    break;
                case 'boolean':
                    $queryBuilder->setParameter($itemParameterName, $item['value'] ? '1' : '0');
                    $ORs[] = $exp->eq(
                        ($this->getField() !== null) ? $this->getField() : $this->column->getSource(),
                        ':' . $itemParameterName
                    );
                    break;
                case 'string':
                case 'integer':
                    $queryBuilder->setParameter($itemParameterName, $item['value']);
                    $ORs[] = $exp->eq(
                        ($this->getField() !== null) ? $this->getField() : $this->column->getSource(),
                        ':' . $itemParameterName
                    );
                    break;
                case 'array':
                    $queryBuilder->setParameter($itemParameterName, $item['value']);
                    $ORs[] = $exp->in(
                        ($this->getField() !== null) ? $this->getField() : $this->column->getSource(),
                        ':' . $itemParameterName
                    );
                    break;
                default:
                    if (($filterExpression =
                            parent::getColumnFilterExpression($queryBuilder, $item['value'], $itemParameterName))
                        && $filterExpression !== null
                    ) {
                        $ORs[] = $filterExpression;
                    };
                    break;
            }

        }

        if (count($ORs) > 0) {
            return $exp->orX()->addMultiple($ORs);
        }

        //Make sure the expression is not added, when there is no or an invalid selection
        return null;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param              $filterString
     * @param              $parameterName
     *
     * @return null
     */
    public function getGlobalFilterExpression(QueryBuilder $queryBuilder, $filterString, $parameterName)
    {
        //Don't filter on global table filter change
        return null;
    }

    /**
     * @return null
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param null $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }


}