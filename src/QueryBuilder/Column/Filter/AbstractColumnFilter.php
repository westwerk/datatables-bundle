<?php


namespace Westwerk\DataTablesBundle\QueryBuilder\Column\Filter;


use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\ORM\QueryBuilder;

abstract class AbstractColumnFilter extends \Westwerk\DataTables\Core\Column\Filter\AbstractColumnFilter implements ColumnFilterInterface
{

    /**
     * @var string|null
     */
    protected $template = null;

    /**
     * @return null|string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param null|string $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param              $filterString
     * @param              $parameterName
     *
     * @return ExpressionBuilder
     */
    public function getColumnFilterExpression(QueryBuilder $queryBuilder, $filterString, $parameterName)
    {
        // Column filter
        $exp = $queryBuilder->expr();

        $match = (isset($this->options['match']) && in_array(strtolower($this->options['match']), ['begin', 'end', 'full']))
            ? $match = strtolower($this->options['match'])
            : null;
        switch ($match) {
            case 'begin':
                $queryBuilder->setParameter($parameterName, $filterString . '%');
                break;
            case 'end':
                $queryBuilder->setParameter($parameterName, '%' . $filterString);
                break;
            case 'full':
                $queryBuilder->setParameter($parameterName, $filterString);
                break;
            default:
                $queryBuilder->setParameter($parameterName, '%' . $filterString . '%');
                break;
        }
        return $exp->like(
            $this->column->getSource(),
            ':' . $parameterName
        );
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param              $filterString
     * @param              $parameterName
     *
     * @return ExpressionBuilder
     */
    public function getGlobalFilterExpression(QueryBuilder $queryBuilder, $filterString, $parameterName)
    {
        return $this->getColumnFilterExpression($queryBuilder, $filterString, $parameterName);
    }

}