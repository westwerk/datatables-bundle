<?php


namespace Westwerk\DataTablesBundle\QueryBuilder\Column\Filter;

use Doctrine\DBAL\Query\Expression\ExpressionBuilder;
use Doctrine\ORM\QueryBuilder;

interface ColumnFilterInterface {

    /**
     * @param QueryBuilder $queryBuilder
     * @param              $filterString
     * @param              $parameterName
     *
     * @return ExpressionBuilder
     */
    public function getColumnFilterExpression(QueryBuilder $queryBuilder, $filterString, $parameterName);

    /**
     * @param QueryBuilder $queryBuilder
     * @param              $filterString
     * @param              $parameterName
     *
     * @return ExpressionBuilder
     */
    public function getGlobalFilterExpression(QueryBuilder $queryBuilder, $filterString, $parameterName);

    /**
     * @return string
     */
    public function getTemplate();

}