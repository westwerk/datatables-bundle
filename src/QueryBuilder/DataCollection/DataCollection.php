<?php

namespace Westwerk\DataTablesBundle\QueryBuilder\DataCollection;

use Westwerk\DataTables\Core\ColumnBuilder\ColumnBuilderInterface;
use Westwerk\DataTables\Core\DataCollection\AbstractDataCollection;
use Westwerk\DataTables\Core\Data\DataRowInterface;
use Westwerk\DataTablesBundle\QueryBuilder\Data\DataRow;

/**
 * Class DataCollection
 *
 * @package Westwerk\DataTablesBundle\QueryBuilder\DataCollection
 */
class DataCollection extends AbstractDataCollection {

    protected $result;

    /**
     * @param ColumnBuilderInterface $columnBuilder
     * @param                        $result
     */
    public function __construct(ColumnBuilderInterface $columnBuilder, $result) {

        parent::__construct($columnBuilder);
        $this->result = $result;
    }

    /**
     * @return DataRowInterface
     */
    public function getNext() {

        $current = current($this->result);
        if($current !== false) {
            $row = new DataRow($this->getColumnBuilder(), $current);
            next($this->result);

            return $row;
        }

        return false;
    }

    public function reset() {

        reset($this->result);

        return $this;
    }
}