<?php

namespace Westwerk\DataTablesBundle\QueryBuilder\Helper;


use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Westwerk\DataTables\Core\Column\ColumnInterface;
use Westwerk\DataTables\Core\ColumnBuilder\ColumnBuilderInterface;
use Westwerk\DataTables\Core\Data\DataColumnInterface;
use Westwerk\DataTables\Core\DataCollection\DataCollectionInterface;

class ExportHelper
{

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var DataCollectionInterface
     */
    protected $dataCollection;

    /**
     * @var ColumnBuilderInterface
     */
    protected $columnBuilder;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var
     */
    protected $filename;

    /**
     * @var
     */
    protected $appendDateTime;

    /**
     * @var
     */
    protected $type;

    /**
     * @var string
     */
    protected $defaultCellDataType = \PHPExcel_Cell_DataType::TYPE_STRING;

    /**
     * @param ContainerInterface $container
     * @param DataCollectionInterface $dataCollection
     * @param Request $request
     */
    public function __construct(
        ContainerInterface $container,
        DataCollectionInterface $dataCollection,
        Request $request
    )
    {
        $this->container = $container;
        $this->dataCollection = $dataCollection;
        $this->request = $request;

        $this->type = $request->get('type', 'xls');
        $this->filename = $request->get('filename', null);
        $this->appendDateTime = (bool)$request->get('appendDateTime', true);

    }

    /**
     * @throws \Exception
     */
    public function processRequest()
    {
        if (!empty($this->type)) {

            /*
             *  PHP7 is a little bit too noisy,
             *  especially while dealing with a harmless PHPExcel type mismatch
             *  in vendor/phpoffice/phpexcel/Classes/PHPExcel/Cell.php on line 835
             */
            error_reporting(error_reporting() & ~E_WARNING);

            $pi = pathinfo($this->filename);
            $filename = $pi['filename'];
            if ($this->appendDateTime) {
                $filename .= '_' . date('Ymd_His');
            }
            $filename .= '.' . $pi['extension'];


            $response = Response::create();
            $headers = array(
                'Content-Description' => 'Datatables Export',
                'Content-Transfer-Encoding' => 'binary',
                'Content-Disposition' => 'attachment; filename="' . $filename . '"',
                'Pragma' => 'no-cache',
                'Expires' => '0',
            );

            set_time_limit(0);

            switch ($this->type) {
                case 'xls':
                    /**
                     * Excel export
                     */
                    $headers['Content-Type'] = 'text/vnd.ms-excel; charset=utf-8';

                    /** @var \PHPExcel $pxl */
                    $pxl = $this->container->get('phpexcel')->createPHPExcelObject();
                    $pxl->setActiveSheetIndex(0);
                    $sheet = $pxl->getActiveSheet();


                    //Write columns to active sheet
                    $rowNo = 1;
                    /** @var Translator $translator */
                    $translator = $this->container->get('translator');
                    $colNo = 0;
                    $colCount = 0;
                    foreach ($this->dataCollection->getColumnBuilder()->getColumns() as $column) {
                        /** @var ColumnInterface $column */
                        if ($column->isExportable()) {
                            $label = $column->getLabel();
                            $label = $translator->trans(!empty($label) ? $label : $column->getName());

                            $cell = $sheet->setCellValueByColumnAndRow($colNo, $rowNo, $label, true);
                            $cell->getStyle()->getFont()->setBold(true);
                            $cell->getStyle()->getBorders()->getBottom()->setBorderStyle(true);

                            $colNo++;
                            $colCount++;
                        }
                    }
                    $sheet->freezePane('A2');


                    while (($row = $this->dataCollection->getNext()) && $row !== false) {
                        $rowNo++;
                        $colNo = 0;
                        while (($column = $row->getNext()) && $column !== false) {
                            /** @var DataColumnInterface $column */
                            if ($column->getDefinition()->isExportable()) {
                                $value = $column->getExportValue();
                                $type = $column->getDefinition()->getExportType();
                                if (is_object($value)) {
                                    throw new \InvalidArgumentException(
                                        'Unexpected type \'object\' for column \'' . $column->getDefinition()
                                            ->getName() . '\': '
                                        . PHP_EOL . print_r($value, true)
                                    );
                                }

                                $type = $this->getCellDataType($value, $type);
                                if (is_array($value)) {
                                    if (count($value) > 1) {
                                        //$sheet->getStyleByColumnAndRow($colNo, $rowNo)->getAlignment()->setWrapText(true);
                                    }
                                    $value = array_reduce($value, function ($carry, $item) use (&$types) {
                                        if (strlen($carry) > 0)
                                            $carry .= "\n";
                                        return $carry . $item;
                                    });
                                }

                                $sheet->setCellValueExplicitByColumnAndRow(
                                    $colNo,
                                    $rowNo,
                                    $value,
                                    $type
                                );
                                $colNo++;
                            }
                        }
                    }

                    // Auto size
                    $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
                    $cellIterator->setIterateOnlyExistingCells(true);
                    /** @var \PHPExcel_Cell $cell */
                    foreach ($cellIterator as $cell) {
                        $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
                    }

                    // Text wrap
                    $pxl->getDefaultStyle()->getAlignment()->setWrapText(true);

                    // Autofilter
                    $sheet->setAutoFilterByColumnAndRow(0,1, $colCount, 1);

                    /** @var \PHPExcel_Writer_Excel5 $writer */
                    $writer = $this->container->get('phpexcel')->createWriter($pxl, 'Excel5');


                    /** @var StreamedResponse $response */
                    $response = $this->container->get('phpexcel')->createStreamedResponse($writer);


                    break;


                default:
                    throw new \Exception('Invalid export type given.');
                    break;
            }

            // Apply headers
            foreach ($headers as $key => $val) {
                $response->headers->set($key, $val);
            }
            return $response;

        }
        throw new \Exception('No export type given.');
    }

    /**
     * @param $value
     *
     * @return string
     */
    protected function getCellDataType($value, $type)
    {
        switch ($type) {
            case ColumnInterface::EXPORT_TYPE_INTEGER:
            case ColumnInterface::EXPORT_TYPE_FLOAT:
                return \PHPExcel_Cell_DataType::TYPE_NUMERIC;

            case ColumnInterface::EXPORT_TYPE_STRING:
                return \PHPExcel_Cell_DataType::TYPE_STRING;

            default:
                return $this->getDefaultCellDataType();
        }
    }

    /**
     * @return string
     */
    public function getDefaultCellDataType()
    {
        return $this->defaultCellDataType;
    }

    /**
     * @param string $defaultCellDataType
     */
    public function setDefaultCellDataType($defaultCellDataType)
    {
        $this->defaultCellDataType = $defaultCellDataType;
    }


}