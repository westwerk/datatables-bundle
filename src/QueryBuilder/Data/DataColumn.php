<?php

namespace Westwerk\DataTablesBundle\QueryBuilder\Data;

use Westwerk\DataTables\Core\Data\AbstractDataColumn;
use Westwerk\DataTables\Core\Column\ColumnInterface;

/**
 * Class DataColumn
 *
 * @package Westwerk\DataTablesBundle\QueryBuilder\Data
 */
class DataColumn extends AbstractDataColumn
{

    /**
     * @var
     */
    protected $entity;

    /**
     * @param ColumnInterface $column
     * @param                 $entity
     */
    public function __construct(ColumnInterface $column, $entity)
    {

        parent::__construct($column);
        $this->setEntity($entity);
    }

    /**
     * @return mixed|void
     */
    public function getValue()
    {

        $columnDefinition = $this->getDefinition();
        if ($columnDefinition->getOnGetValue() !== null) {
            if (is_callable($columnDefinition->getOnGetValue())) {
                return call_user_func($columnDefinition->getOnGetValue(), $columnDefinition, $this->getEntity());
            } else {
                return $columnDefinition->getOnGetValue();
            }
        } elseif (!$columnDefinition->isVirtual()) {
            $identifier = $columnDefinition->getIdentifier();
            return $this->getEntity()[$identifier];
        } else {
            return null;
        }
    }

    /**
     * @return mixed|void
     */
    public function getExportValue()
    {
        $columnDefinition = $this->getDefinition();
        if ($columnDefinition->getOnGetExportValue() !== null) {
            if (is_callable($columnDefinition->getOnGetExportValue())) {
                return call_user_func($columnDefinition->getOnGetExportValue(), $columnDefinition, $this->getEntity());
            } else {
                return $columnDefinition->getOnGetExportValue();
            }
        } elseif (!$columnDefinition->isVirtual()) {
            $identifier = $columnDefinition->getIdentifier();

            return $this->getEntity()[$identifier];
            //return preg_replace('/<\s*br\s*\/?\s*>/',PHP_EOL,strip_tags($this->getValue()));
        } else {
            return null;
        }
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {

        return $this->entity;
    }

    /**
     * @param mixed $entity
     */
    protected function setEntity($entity)
    {

        $this->entity = $entity;
    }
}