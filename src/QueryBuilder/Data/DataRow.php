<?php

namespace Westwerk\DataTablesBundle\QueryBuilder\Data;

use Westwerk\DataTables\Core\ColumnBuilder\ColumnBuilderInterface;
use Westwerk\DataTables\Core\Data\AbstractDataRow;

/**
 * Class DataRow
 *
 * @package Westwerk\DataTablesBundle\Data\QueryBuilder
 */
class DataRow extends AbstractDataRow {

    /**
     * @var mixed
     */
    protected $entity;

    /**
     * @param ColumnBuilderInterface $columnBuilder
     * @param                        $entity mixed
     */
    public function __construct(ColumnBuilderInterface $columnBuilder, $entity) {

        parent::__construct($columnBuilder);
        $this->setEntity($entity);
        $this->prepareColumns();
    }

    /**
     * @return mixed
     */
    public function getEntity() {

        return $this->entity;
    }

    /**
     * @param mixed $entity
     */
    public function setEntity($entity) {

        $this->entity = $entity;
    }

    protected function prepareColumns() {

        $entity = $this->getEntity();
        foreach($this->getColumnBuilder()->getColumns() as $column) {
            $this->addColumn(new DataColumn($column, $entity));
        }
    }
}