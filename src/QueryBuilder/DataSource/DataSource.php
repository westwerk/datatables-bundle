<?php

namespace Westwerk\DataTablesBundle\QueryBuilder\DataSource;

use Doctrine\ORM\Query\Expr;
use Westwerk\DataTables\Core\DataSource\AbstractDataSource;
use Westwerk\DataTablesBundle\QueryBuilder\Column\Filter\ColumnFilterInterface;
use Westwerk\DataTablesBundle\QueryBuilder\Column\Column;
use Westwerk\DataTablesBundle\QueryBuilder\DataCollection\DataCollection;
use Doctrine\ORM\QueryBuilder;
use Westwerk\DataTables\Core\Column\ColumnInterface;

/**
 * Class DataSource
 *
 * @package Westwerk\DataTablesBundle\QueryBuilder\DataSource
 */
class DataSource extends AbstractDataSource
{

    /**
     * @var QueryBuilder
     */
    protected $queryBuilder;

    /**
     * @var bool
     */
    protected $filterApplied = false;

    /**
     * @var null|callable
     */
    protected $onBeforePrepare = null;

    /**
     * @var null|callable
     */
    protected $onAfterPrepare = null;

    /**
     * @var null|callable
     */
    protected $onBeforePrepareSorting = null;

    /**
     * @var null|callable
     */
    protected $onAfterPrepareSorting = null;

    /**
     * @var null|callable
     */
    protected $onBeforePrepareFilter = null;

    /**
     * @var null|callable
     */
    protected $onAfterPrepareFilter = null;


    public function __construct(QueryBuilder $queryBuilder)
    {

        $this->setQueryBuilder($queryBuilder);
    }

    /**
     * @return mixed
     */
    public function prepare()
    {

        $qb = clone $this->queryBuilder;

        $this->on($this->getOnBeforePrepare(), $qb);

        // Setup select fields
        $this->prepareSelectFields($qb);

        // Total record count
        $this->setTotalRecords($this->calcTotalRecordCount($qb));

        // Filter
        $this->on($this->getOnBeforePrepareFilter(), $qb);
        $this->prepareFilter($qb);
        $this->on($this->getOnAfterPrepareFilter(), $qb);

        // Total display record count
        if ($this->isFilterApplied()) {
            $this->setTotalDisplayRecords($this->calcTotalDisplayRecordCount($qb));
        } else {
            $this->setTotalDisplayRecords($this->getTotalRecords());
        }

        // Sorting
        $this->on($this->getOnBeforePrepareSorting(), $qb);
        $this->prepareSorting($qb);
        $this->on($this->getOnAfterPrepareSorting(), $qb);

        // Limit result set
        if ($this->getMaxResults() > 0) {
            $this->prepareLimits($qb);
        }

        $this->on($this->getOnAfterPrepare(), $qb);

        $this->setDataCollection(new DataCollection($this->getColumnBuilder(), $qb->getQuery()->execute()));
    }

    /**
     * @param QueryBuilder $qb
     */
    protected function prepareSorting(QueryBuilder $qb)
    {

        foreach ($this->getColumnBuilder()->getColumns() as $column) {
            /** @var Column $column */
            if ($column->isSorted()) {
                if ($column->getOnSort() !== null && is_callable($column->getOnSort())) {
                    call_user_func($column->getOnSort(), $qb, $column);
                } else {
                    $qb->addOrderBy(
                        $column->getIdentifier(),
                        ($column->getSortDirection() === ColumnInterface::SORT_DIRECTION_ASC ? 'ASC' : 'DESC')
                    );
                }
            }
        }
    }

    /**
     * @param QueryBuilder $qb
     */
    protected function prepareFilter(QueryBuilder $qb)
    {

        // Check, if there are any filters set
        foreach ($this->getColumnBuilder()->getColumns() as $column) {
            /** @var Column $column */
            if (($this->isFiltered() || ($column->hasFilter() && $column->isFiltered()))) {
                $this->setFilterApplied(true);
                break;
            }
        }

        if ($this->isFilterApplied()) {
            if ($this->on($this->getOnFilter(), $qb, $this) === false) {
                $exp        = $qb->expr();
                $globalExps = array();
                $columnExps = array();
                foreach ($this->getColumnBuilder()->getColumns() as $column) {
                    /** @var Column $column */
                    if ($column->hasFilter()) {
                        if ($this->isFiltered() || $column->isFiltered()) {
                            if (!$this->on($column->getOnFilter(), $qb, $this, $column)) {
                                if ($column->isFiltered()) {
                                    // Column filter
                                    $columnExps = $this->applyColumnFilter($columnExps, $qb, $column);
                                }
                                if ($this->isFiltered()) {
                                    // Global filter
                                    $globalExps = $this->applyGlobalFilter($globalExps, $qb, $column);
                                }
                            }
                        }
                    }
                }
                if (count($globalExps) > 0) {
                    $qb->andWhere($exp->orX()->addMultiple($globalExps));
                }
                if (count($columnExps) > 0) {
                    $qb->andWhere($exp->andX()->addMultiple($columnExps));
                }
            }
        }
    }

    /**
     * @param $callback
     * @param $arguments
     *
     * @return bool
     */
    protected function on($callback, ...$arguments)
    {
        if ($callback !== null && is_callable($callback)) {
            return call_user_func($callback, ...$arguments);
        }
        return false;
    }

    /**
     * @param array              $exps
     * @param QueryBuilder       $qb
     * @param Column $column
     *
     * @return array
     */
    protected function applyColumnFilter(array $exps, QueryBuilder $qb, Column $column)
    {
        $filterString    = $column->getFilterString();
        $parameterName   = 'filterString_' . str_replace('.','_',$column->getIdentifier());
        $filterExpession = null;
        if (($filterExpression = $this->on(
                $column->getOnColumnFilterExpression(),
                $qb,
                $column,
                $filterString,
                $parameterName
            )) === false
        ) {
            if ($filterString === ColumnInterface::FILTER_VALUE_NULLABLE) {
                $filterExpression = $qb->expr()->isNull($column->getSource());
            } else {
                $filterExpression = $column->getFilter()->getColumnFilterExpression($qb, $filterString, $parameterName);
            }
        }
        if ($filterExpression !== null) {
            $exps[] = $filterExpression;
        }
        return $exps;
    }


    protected function applyGlobalFilter(array $exps, QueryBuilder $qb, Column $column)
    {
        $filterString     = $this->getFilterString();
        $parameterName    = 'globalFilterString';
        $filterExpression = null;
        if (($filterExpression = $this->on(
                $column->getOnGlobalFilterExpression(),
                $qb,
                $column,
                $filterString,
                $parameterName
            )) === false
        ) {
            $filterExpression = $column->getFilter()->getGlobalFilterExpression($qb, $filterString, $parameterName);
        }
        if ($filterExpression !== null) {
            $exps[] = $filterExpression;
        }
        return $exps;
    }


    /**
     * @param QueryBuilder $qb
     *
     * @return mixed
     * @throws \Exception
     */
    protected function calcTotalDisplayRecordCount(QueryBuilder $qb)
    {

        $countQb = clone $qb;
        if ($this->on($this->getOnCountTotalDisplayRecords(), $countQb) === false) {
            $countQb->resetDQLPart('groupBy');
            $countQb->resetDQLPart('orderBy');
            $dqlParts = $countQb->getDQLPart('from');
            $aliases  = array_map(
                function ($arr) {

                    return $arr->getAlias();
                },
                $dqlParts
            );
            if (count($aliases) == 1) {
                $countQb->select('COUNT(DISTINCT ' . $aliases[0] . ')');
            } else {
                throw new \Exception(
                    'Calculating the total number of records with more than one from DQL part set is currently not supported.'
                );
            }
        }

        return $countQb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return mixed
     * @throws \Exception
     */
    protected function calcTotalRecordCount(QueryBuilder $qb)
    {

        $countQb = clone $qb;
        if ($this->on($this->getOnCountTotalRecords(), $countQb) === false) {
            $countQb->resetDQLPart('groupBy');
            $countQb->resetDQLPart('orderBy');
            $dqlParts = $countQb->getDQLPart('from');
            $aliases  = array_map(
                function ($arr) {

                    return $arr->getAlias();
                },
                $dqlParts
            );
            if (count($aliases) == 1) {
                $countQb->select('COUNT(DISTINCT ' . $aliases[0] . ')');
            } else {
                throw new \Exception(
                    'Calculating the total number of records with more than one from DQL part set is currently not supported.'
                );
            }
        }

        return $countQb->getQuery()->getSingleScalarResult();
    }

    /**
     * Add select fields
     *
     * @param $qb QueryBuilder
     */
    protected function prepareSelectFields(QueryBuilder $qb)
    {

        foreach ($this->columnBuilder->getColumns() as $key => $column) {
            /** @var Column $column */
            if (!$column->isVirtual()) {
                if ($column->getOnSelect() !== null && is_callable($column->getOnSelect())) {
                    call_user_func($column->getOnSelect(), $qb, $column);
                } elseif ($column->getSource() !== null) {
                    $identifier = $column->getIdentifier();
                    $qb->addSelect($column->getSource() . ' AS ' . $identifier);
                }
            }
        }
    }

    /**
     * @param QueryBuilder $qb
     */
    protected function prepareLimits(QueryBuilder $qb)
    {

        $qb->setMaxResults($this->getMaxResults());
        $qb->setFirstResult($this->getFirstResult());
    }

    /**
     * @return QueryBuilder
     */
    public function getQueryBuilder()
    {

        return $this->queryBuilder;
    }

    /**
     * @param QueryBuilder $queryBuilder
     */
    public function setQueryBuilder($queryBuilder)
    {

        $this->queryBuilder = $queryBuilder;
    }

    /**
     * @return boolean
     */
    public function isFilterApplied()
    {

        return $this->filterApplied;
    }

    /**
     * @param boolean $filterApplied
     */
    public function setFilterApplied($filterApplied)
    {

        $this->filterApplied = $filterApplied;
    }

    /**
     * @return callable|null
     */
    public function getOnBeforePrepare()
    {
        return $this->onBeforePrepare;
    }

    /**
     * @param callable|null $onBeforePrepare
     */
    public function setOnBeforePrepare($onBeforePrepare)
    {
        $this->onBeforePrepare = $onBeforePrepare;
    }

    /**
     * @return callable|null
     */
    public function getOnAfterPrepare()
    {
        return $this->onAfterPrepare;
    }

    /**
     * @param callable|null $onAfterPrepare
     */
    public function setOnAfterPrepare($onAfterPrepare)
    {
        $this->onAfterPrepare = $onAfterPrepare;
    }

    /**
     * @return callable|null
     */
    public function getOnBeforePrepareSorting()
    {
        return $this->onBeforePrepareSorting;
    }

    /**
     * @param callable|null $onBeforePrepareSorting
     */
    public function setOnBeforePrepareSorting($onBeforePrepareSorting)
    {
        $this->onBeforePrepareSorting = $onBeforePrepareSorting;
    }

    /**
     * @return callable|null
     */
    public function getOnAfterPrepareSorting()
    {
        return $this->onAfterPrepareSorting;
    }

    /**
     * @param callable|null $onAfterPrepareSorting
     */
    public function setOnAfterPrepareSorting($onAfterPrepareSorting)
    {
        $this->onAfterPrepareSorting = $onAfterPrepareSorting;
    }

    /**
     * @return callable|null
     */
    public function getOnBeforePrepareFilter()
    {
        return $this->onBeforePrepareFilter;
    }

    /**
     * @param callable|null $onBeforePrepareFilter
     */
    public function setOnBeforePrepareFilter($onBeforePrepareFilter)
    {
        $this->onBeforePrepareFilter = $onBeforePrepareFilter;
    }

    /**
     * @return callable|null
     */
    public function getOnAfterPrepareFilter()
    {
        return $this->onAfterPrepareFilter;
    }

    /**
     * @param callable|null $onAfterPrepareFilter
     */
    public function setOnAfterPrepareFilter($onAfterPrepareFilter)
    {
        $this->onAfterPrepareFilter = $onAfterPrepareFilter;
    }


}