<?php

namespace Westwerk\DataTablesBundle\QueryBuilder\DataTable;

use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Westwerk\DataTables\Core\Column\ColumnInterface;
use Westwerk\DataTables\Core\DataAggregator\DataAggregator;
use Westwerk\DataTables\Core\DataSource\DataSourceInterface;
use Westwerk\DataTablesBundle\QueryBuilder\DataSource\DataSource;
use Doctrine\ORM\QueryBuilder;
use Westwerk\DataTables\Core\DataTable\AbstractDataTable;
use Westwerk\DataTables\Core\DataAggregator\DataAggregatorInterface;
use Westwerk\DataTablesBundle\QueryBuilder\Helper\ExportHelper;

/**
 * Class DataTable
 *
 * @package Westwerk\DataTablesBundle\DataTable
 * @author  Westwerk GmbH & Co. KG <web@westwerk.ac>
 */
abstract class DataTable extends AbstractDataTable
{

    /**
     * @var string
     */
    protected $template = "@WestwerkDataTables/datatable.html.twig";

    /**
     * @var \Twig_Environment
     */
    protected $templating;

    /**
     * The Translator service.
     *
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * The Router service.
     *
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var QueryBuilder
     */
    protected $queryBuilder;


    /**
     * @var string
     */
    protected $dataUri;

    /**
     * @var null|DataSource
     */
    protected $dataSource = null;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(
        ContainerInterface $container
    )
    {
        $this->container = $container;
        $this->templating = $container->get('twig');
        $this->translator = $container->get('translator');
        /** @var Router router */
        $this->router = $container->get('router');
        /** @var Request request */
        $this->request = $container->get('request_stack')->getCurrentRequest();

        $this->setDataUri($this->request->getUri());

        parent::__construct();
    }

    /**
     * @param array $options
     *
     * @return mixed|string|void
     * @throws \Exception
     * @throws \Twig_Error
     */
    public function render(array $options = array())
    {
        $this->setOptions($options);
        $columns = $this->getVisibleColumns();
        $args = array(
            // Columns
            'columns' => $columns,
            // $this
            'datatable' => $this,
            // Datatable init options
            'options' => array_merge_recursive(
                array(
                    'stateSave' => true,
                    'serverSide' => true,
                    'ajax' => array(
                        'url' => $this->getDataUri(),
                        'type' => 'POST',
                    ),
                    'columns' => array_map(
                        function ($column) {
                            /** @var ColumnInterface $column */
                            return array_merge_recursive(
                                array(
                                    'orderable' => $column->isSortable()
                                ),
                                $column->getOptions()
                            ); // Merge with column options
                        },
                        $columns
                    ),
                ),
                $this->getOptions()
            ) // Merge with datatables options
        );
        return $this->templating->render($this->getTemplate(), $args);
    }

    /**
     * @inheritdoc
     */
    public function processExportRequest()
    {
        $aggregator = $this->getDataAggregator();
        $dataSource = $this->getDataSource();

        $afterPrepare = $dataSource->getOnAfterPrepare();
        $dataSource->setOnAfterPrepare(function(QueryBuilder $qb) use ($afterPrepare) {
            if (is_callable($afterPrepare)) {
                call_user_func($afterPrepare, $qb);
            }
            $qb->setFirstResult(0);
            $qb->setMaxResults(null);
        });

        $this->setupDataSource($dataSource);
        $aggregator->setDataSource($dataSource);

        $exportHelper =
            new ExportHelper($this->container, $aggregator->getDataCollection(), $this->request);
        return $exportHelper->processRequest();
    }

    /**
     * @inheritdoc
     */
    public function processRequest()
    {
        // Do we have an export request here?
        if ((bool)$this->request->get('export', false) === true) {
            return $this->processExportRequest();
        }

        // Normal request for table data
        try {
            $aggregator = $this->getDataAggregator();
            $dataSource = $this->getDataSource();
            $this->setupDataSource($dataSource);
            $aggregator->setDataSource($dataSource);
            return Response::create(json_encode($aggregator->getData()));
        } catch (\Exception $e) {

            // Let's invoke bugsnag
            try {
                if ($this->container->has('bugsnag.clientloader')) {
                    $client = $this->container->get('bugsnag.clientloader');
                    if (!($e instanceof HttpException)) {
                        $client->notifyOnException($e);
                    }
                }
            } catch (\Exception $e) {
                throw $e;
            }

            return Response::create(json_encode(
                [
                    'data' => [],
                    'iTotalRecords' => 0,
                    'iTotalDisplayRecords' => 0,
                    'exception' => $e->getMessage().PHP_EOL.$e->getTraceAsString()
                ]
            ));
        }
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param string $template
     *
     * @return $this
     */
    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDataRequest()
    {
        return (
            (!is_null($this->request->get('draw', null)) && is_array($this->request->get('columns')))
            || $this->request->isXmlHttpRequest()
        );
    }

    /**
     * @return \Twig_Environment
     */
    protected function getTemplating()
    {
        return $this->templating;
    }

    /**
     * @return ContainerInterface
     */
    protected function getContainer()
    {
        return $this->container;
    }

    /**
     * @return string
     */
    public function getDataUri()
    {
        return $this->dataUri;
    }

    /**
     * @param string $dataUri
     *
     * @return $this
     */
    public function setDataUri($dataUri)
    {
        $this->dataUri = $dataUri;
        return $this;
    }

    /**
     * @param QueryBuilder $queryBuilder
     */
    public function setupQueryBuilder(QueryBuilder $queryBuilder)
    {
    }

    /**
     * @param DataSourceInterface $dataAggregator
     *
     * @return mixed|void
     */
    public function setupDataSource(DataSourceInterface $dataAggregator)
    {
    }

    /**
     * @return QueryBuilder
     */
    public function getQueryBuilder()
    {

        return $this->queryBuilder;
    }

    /**
     * @param QueryBuilder $queryBuilder
     */
    public function setQueryBuilder($queryBuilder)
    {

        $this->queryBuilder = $queryBuilder;
    }

    /**
     * @return DataAggregatorInterface
     */
    public function getDataAggregator()
    {
        if ($this->dataAggregator === null) {
            $this->dataAggregator = new DataAggregator($this->getColumnBuilder(), $this->request);
        }
        return $this->dataAggregator;
    }


    /**
     * @return null|DataSource
     */
    public function getDataSource()
    {
        if ($this->dataSource === null) {
            $queryBuilder = $this->getQueryBuilder();
            $this->setupQueryBuilder($queryBuilder);
            $this->dataSource = new DataSource($queryBuilder);
        }
        return $this->dataSource;
    }


}
