<?php

namespace Westwerk\DataTablesBundle\Twig;

use Twig_Extension;
use Twig_SimpleFunction;
use Exception;
use Symfony\Component\Translation\TranslatorInterface;
use Westwerk\DataTables\Core\DataTable\DataTableInterface;
use Westwerk\DataTablesBundle\QueryBuilder\Column\Filter\ColumnFilterInterface;

/**
 * Class DatatableTwigExtension
 *
 * @package Westwerk\DataTablesBundle\Twig
 */
class DataTableTwigExtension extends Twig_Extension
{

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {

        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {

        return "westwerk_datatables_twig_extension";
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {

        return array(
            new Twig_SimpleFunction(
                "datatable",
                array($this, "render"),
                array("is_safe" => array("all"), 'needs_environment' => true)
            ),
        );
    }

    /**
     * @param \Twig_Environment $twig
     * @param DataTableInterface $datatable
     * @param array $options
     *
     * @return mixed
     */
    public function render(\Twig_Environment $twig, DataTableInterface $datatable, array $options = array())
    {
        return $datatable->render($options);
    }

}
