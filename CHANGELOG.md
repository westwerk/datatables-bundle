# Changelog

## 1.0.2 / 13/06/2017

* Fixes an export issue with newer PHP versions or versions that are configured to show warnings
* Added dependency for PHPExcel

## 1.0.1 / 01/06/2017

* Fixed dependency version

## 1.0.0 / 01/06/2017

* First release